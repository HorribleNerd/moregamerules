package com.horriblenerd.moregamerules;

import net.minecraft.world.GameRules;
import net.minecraftforge.fml.DeferredWorkQueue;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Setup {

    static Method booleanCreate;
    static Method integerCreate;

    public static void setup(final FMLCommonSetupEvent event) {
        try {
            booleanCreate = ObfuscationReflectionHelper.findMethod(GameRules.BooleanValue.class, "func_223568_b", boolean.class);
            booleanCreate.setAccessible(true);

            integerCreate = ObfuscationReflectionHelper.findMethod(GameRules.IntegerValue.class, "func_223559_b", int.class);
            integerCreate.setAccessible(true);

            MoreGamerules.LOGGER.debug("Adding new gamerules...");
            DeferredWorkQueue.runLater(() -> addBooleanRule(MoreGamerules.ENDERMAN_GRIEFING));
            DeferredWorkQueue.runLater(() -> addBooleanRule(MoreGamerules.CREEPER_GRIEFING));
            DeferredWorkQueue.runLater(() -> addBooleanRule(MoreGamerules.GHAST_GRIEFING));
            DeferredWorkQueue.runLater(() -> addBooleanRule(MoreGamerules.DO_PET_FRIENDLY_FIRE));
            DeferredWorkQueue.runLater(() -> addBooleanRule(MoreGamerules.DO_FARMLAND_TRAMPLE));
            DeferredWorkQueue.runLater(() -> addBooleanRule(MoreGamerules.ENDERPEARL_DAMAGE));
            DeferredWorkQueue.runLater(() -> addBooleanRule(MoreGamerules.DO_ITEM_DESPAWN));
            DeferredWorkQueue.runLater(() -> addBooleanRule(MoreGamerules.DO_CRITICAL_HITS));
            DeferredWorkQueue.runLater(() -> addBooleanRule(MoreGamerules.DO_ENDERMAN_TELEPORT));
            DeferredWorkQueue.runLater(() -> addBooleanRule(MoreGamerules.DO_SHULKER_TELEPORT));
            DeferredWorkQueue.runLater(() -> addBooleanRule(MoreGamerules.DO_BABY_ZOMBIE_SPAWNING));
            DeferredWorkQueue.runLater(() -> addBooleanRule(MoreGamerules.DO_BAT_SPAWNING));
            DeferredWorkQueue.runLater(() -> addBooleanRule(MoreGamerules.DO_CROP_GROW));
            DeferredWorkQueue.runLater(() -> addBooleanRule(MoreGamerules.DO_SAPLING_GROW));
            DeferredWorkQueue.runLater(() -> addBooleanRule(MoreGamerules.DISABLE_HUNGER));
            DeferredWorkQueue.runLater(() -> addIntegerRule(MoreGamerules.MAX_HEALTH));
            DeferredWorkQueue.runLater(() -> addBooleanRule(MoreGamerules.DO_MONSTER_SPAWNING));


        }
        catch (IllegalArgumentException e) {
            MoreGamerules.LOGGER.error("IllegalArgumentException during FMLClientSetupEvent.");
            e.printStackTrace();
            throw e;
        }
    }

    private static void addBooleanRule(MoreGamerules.Rule.BooleanRule rule) {
        try {
            Object val = booleanCreate.invoke(GameRules.BooleanValue.class, rule.getDefaultValue());
            rule.setRule(GameRules.func_234903_a_(rule.getName(), rule.getCategory(), (GameRules.RuleType<GameRules.BooleanValue>) val));
            MoreGamerules.LOGGER.debug("Added new gamerule: " + rule.getName());
        } catch (IllegalAccessException e) {
            MoreGamerules.LOGGER.error("IllegalAccessException during FMLClientSetupEvent.");
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            MoreGamerules.LOGGER.error("InvocationTargetException during FMLClientSetupEvent.");
            e.printStackTrace();
        }
    }

    private static void addIntegerRule(MoreGamerules.Rule.IntegerRule rule) {
        try {
            Object val = integerCreate.invoke(GameRules.IntegerValue.class, rule.getDefaultValue());
            rule.setRule(GameRules.func_234903_a_(rule.getName(), rule.getCategory(), (GameRules.RuleType<GameRules.IntegerValue>) val));
            MoreGamerules.LOGGER.debug("Added new gamerule: " + rule.getName());
        } catch (IllegalAccessException e) {
            MoreGamerules.LOGGER.error("IllegalAccessException during FMLClientSetupEvent.");
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            MoreGamerules.LOGGER.error("InvocationTargetException during FMLClientSetupEvent.");
            e.printStackTrace();
        }

    }

}
