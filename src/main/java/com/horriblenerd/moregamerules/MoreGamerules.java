package com.horriblenerd.moregamerules;

import net.minecraft.world.GameRules;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod("moregamerules")
public class MoreGamerules {
    public static final String MODID = "moregamerules";
    public static final Logger LOGGER = LogManager.getLogger();

    // MOBS
    public static Rule.BooleanRule ENDERMAN_GRIEFING = new Rule.BooleanRule("endermanGriefing", GameRules.Category.MOBS, true);
    public static Rule.BooleanRule CREEPER_GRIEFING = new Rule.BooleanRule("creeperGriefing", GameRules.Category.MOBS, true);
    public static Rule.BooleanRule GHAST_GRIEFING = new Rule.BooleanRule("ghastGriefing", GameRules.Category.MOBS, true);
    public static Rule.BooleanRule DO_PET_FRIENDLY_FIRE = new Rule.BooleanRule("petFriendlyFire", GameRules.Category.MOBS, true);
    public static Rule.BooleanRule DO_ENDERMAN_TELEPORT = new Rule.BooleanRule("doEndermanTeleport", GameRules.Category.MOBS, true);
    public static Rule.BooleanRule DO_SHULKER_TELEPORT = new Rule.BooleanRule("doShulkerTeleport", GameRules.Category.MOBS, true);

    // SPAWNING
    public static Rule.BooleanRule DO_BABY_ZOMBIE_SPAWNING = new Rule.BooleanRule("doBabyZombieSpawning", GameRules.Category.SPAWNING, true);
    public static Rule.BooleanRule DO_BAT_SPAWNING = new Rule.BooleanRule("doBatSpawning", GameRules.Category.SPAWNING, true);
    public static Rule.BooleanRule DO_MONSTER_SPAWNING = new Rule.BooleanRule("doMonsterSpawning", GameRules.Category.SPAWNING, true);

    // UPDATES
    public static Rule.BooleanRule DO_FARMLAND_TRAMPLE = new Rule.BooleanRule("doFarmlandTrample", GameRules.Category.UPDATES, true);
    public static Rule.BooleanRule DO_CROP_GROW = new Rule.BooleanRule("doCropGrow", GameRules.Category.UPDATES, true);
    public static Rule.BooleanRule DO_SAPLING_GROW = new Rule.BooleanRule("doSaplingGrow", GameRules.Category.UPDATES, true);

    // PLAYER
    public static Rule.BooleanRule ENDERPEARL_DAMAGE = new Rule.BooleanRule("enderpearlDamage", GameRules.Category.PLAYER, true);
    public static Rule.BooleanRule DO_CRITICAL_HITS = new Rule.BooleanRule("doCriticalHits", GameRules.Category.PLAYER, true);
    public static Rule.BooleanRule DISABLE_HUNGER = new Rule.BooleanRule("disableHunger", GameRules.Category.PLAYER, false); // Suggested by rickyybrez
    public static Rule.IntegerRule MAX_HEALTH = new Rule.IntegerRule("maxPlayerHealth", GameRules.Category.PLAYER, 20);

    // DROPS
    public static Rule.BooleanRule DO_ITEM_DESPAWN = new Rule.BooleanRule("doItemDespawn", GameRules.Category.DROPS, true);


    public MoreGamerules() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(Setup::setup);
        MinecraftForge.EVENT_BUS.register(new ForgeEventHandlers());
    }

    public static class Rule {

        private final String name;
        private final GameRules.Category category;
        private GameRules.RuleKey<?> rule;

        public Rule(String name, GameRules.Category category) {

            this.name = name;
            this.category = category;
        }

        public String getName() {
            return name;
        }

        public GameRules.Category getCategory() {
            return category;
        }

        public GameRules.RuleKey<?> getRule() {
            return rule;
        }

        public void setRule(GameRules.RuleKey<?> ruleKey) {
            this.rule = ruleKey;
        }

        public static class BooleanRule extends Rule {

            private final boolean defaultValue;

            public BooleanRule(String name, GameRules.Category category, boolean defaultValue) {
                super(name, category);
                this.defaultValue = defaultValue;
            }

            @Override
            public GameRules.RuleKey<GameRules.BooleanValue> getRule() {
                return (GameRules.RuleKey<GameRules.BooleanValue>) super.getRule();
            }

            public boolean getDefaultValue() {
                return defaultValue;
            }

        }

        public static class IntegerRule extends Rule {

            private final int defaultValue;

            public IntegerRule(String name, GameRules.Category category, int defaultValue) {
                super(name, category);
                this.defaultValue = defaultValue;
            }

            @Override
            public GameRules.RuleKey<GameRules.IntegerValue> getRule() {
                return (GameRules.RuleKey<GameRules.IntegerValue>) super.getRule();
            }

            public int getDefaultValue() {
                return defaultValue;
            }

        }

    }

}
