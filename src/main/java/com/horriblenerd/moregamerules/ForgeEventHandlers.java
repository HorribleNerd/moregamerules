package com.horriblenerd.moregamerules;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.monster.*;
import net.minecraft.entity.passive.BatEntity;
import net.minecraft.entity.passive.CatEntity;
import net.minecraft.entity.passive.ParrotEntity;
import net.minecraft.entity.passive.WolfEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.FoodStats;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.event.CommandEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.EntityMobGriefingEvent;
import net.minecraftforge.event.entity.item.ItemExpireEvent;
import net.minecraftforge.event.entity.living.EnderTeleportEvent;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingSpawnEvent;
import net.minecraftforge.event.entity.player.CriticalHitEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.event.world.SaplingGrowTreeEvent;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class ForgeEventHandlers {

    @SubscribeEvent
    public void onMobGrief(EntityMobGriefingEvent event) {
        Entity entity = event.getEntity();
        if (entity instanceof EndermanEntity && !entity.getEntityWorld().getGameRules().getBoolean(MoreGamerules.ENDERMAN_GRIEFING.getRule())) {
            event.setResult(Event.Result.DENY);
        }
        else if (entity instanceof CreeperEntity && !entity.getEntityWorld().getGameRules().getBoolean(MoreGamerules.CREEPER_GRIEFING.getRule())) {
            event.setResult(Event.Result.DENY);
        }
        else if (entity instanceof GhastEntity && !entity.getEntityWorld().getGameRules().getBoolean(MoreGamerules.GHAST_GRIEFING.getRule())) {
            event.setResult(Event.Result.DENY);
        }
    }

    @SubscribeEvent
    public void onLivingDamage(LivingDamageEvent event) {
        LivingEntity target = event.getEntityLiving();
        Entity source = event.getSource().getTrueSource();
        if (target instanceof WolfEntity && source instanceof PlayerEntity) {
            event.setCanceled(((WolfEntity) target).isOwner((PlayerEntity) source) && source.getEntityWorld().getGameRules().getBoolean(MoreGamerules.DO_PET_FRIENDLY_FIRE.getRule()));
        }
        else if (target instanceof CatEntity && source instanceof PlayerEntity) {
            event.setCanceled(((CatEntity) target).isOwner((PlayerEntity) source) && source.getEntityWorld().getGameRules().getBoolean(MoreGamerules.DO_PET_FRIENDLY_FIRE.getRule()));
        }
        else if (target instanceof ParrotEntity && source instanceof PlayerEntity) {
            event.setCanceled(((ParrotEntity) target).isOwner((PlayerEntity) source) && source.getEntityWorld().getGameRules().getBoolean(MoreGamerules.DO_PET_FRIENDLY_FIRE.getRule()));
        }
    }

    @SubscribeEvent
    public void onTeleport(EnderTeleportEvent event) {
        if (event.getEntity() instanceof PlayerEntity && !event.getEntity().getEntityWorld().getGameRules().getBoolean(MoreGamerules.ENDERPEARL_DAMAGE.getRule())) {
            event.setAttackDamage(0F);
        }
        else if (event.getEntity() instanceof EndermanEntity && !event.getEntity().getEntityWorld().getGameRules().getBoolean(MoreGamerules.DO_ENDERMAN_TELEPORT.getRule())) {
            event.setCanceled(true);
        }
        else if (event.getEntity() instanceof ShulkerEntity && !event.getEntity().getEntityWorld().getGameRules().getBoolean(MoreGamerules.DO_SHULKER_TELEPORT.getRule())) {
            event.setCanceled(true);
        }
    }

    @SubscribeEvent
    public void onItemExpire(ItemExpireEvent event) {
        ItemEntity itemEntity = (ItemEntity) event.getEntity();
        event.setCanceled(!(event.getEntity().getEntityWorld().getGameRules().getBoolean(MoreGamerules.DO_ITEM_DESPAWN.getRule())) && !itemEntity.cannotPickup());
    }

    @SubscribeEvent
    public void onCriticalHit(CriticalHitEvent event) {
        if(!event.getEntity().getEntityWorld().getGameRules().getBoolean(MoreGamerules.DO_CRITICAL_HITS.getRule())) {
            event.setResult(Event.Result.DENY);
        }
    }

    @SubscribeEvent
    public void onFarmlandTrample(BlockEvent.FarmlandTrampleEvent event) {
        event.setCanceled(!event.getEntity().getEntityWorld().getGameRules().getBoolean(MoreGamerules.DO_FARMLAND_TRAMPLE.getRule()));
    }

    @SubscribeEvent
    public void onCropGrow(BlockEvent.CropGrowEvent event) {
        if (!((World) event.getWorld()).getGameRules().getBoolean(MoreGamerules.DO_CROP_GROW.getRule())) event.setResult(Event.Result.DENY);
    }

    @SubscribeEvent
    public void onSaplingGrow(SaplingGrowTreeEvent event) {
        if (!((World) event.getWorld()).getGameRules().getBoolean(MoreGamerules.DO_SAPLING_GROW.getRule())) event.setResult(Event.Result.DENY);
    }

    @SubscribeEvent
    public void onSpawnEvent(LivingSpawnEvent.SpecialSpawn event) {
        LivingEntity entity = event.getEntityLiving();
        if (entity instanceof BatEntity && !entity.getEntityWorld().getGameRules().getBoolean(MoreGamerules.DO_BAT_SPAWNING.getRule())) {
            event.setCanceled(true);
        }
        if (entity instanceof MonsterEntity && !entity.getEntityWorld().getGameRules().getBoolean(MoreGamerules.DO_MONSTER_SPAWNING.getRule())) {
            event.setCanceled(true);
        }
    }

    @SubscribeEvent
    public void onCheckSpawn(LivingSpawnEvent.CheckSpawn event) {
        LivingEntity entity = event.getEntityLiving();
        if (entity instanceof MonsterEntity && !entity.getEntityWorld().getGameRules().getBoolean(MoreGamerules.DO_MONSTER_SPAWNING.getRule())) {
            event.setResult(Event.Result.DENY);
        }
    }

    @SubscribeEvent
    public void onEntityJoinWorld(EntityJoinWorldEvent event) {
        Entity entity = event.getEntity();
        if (entity instanceof ZombieEntity && !entity.getEntityWorld().getGameRules().getBoolean(MoreGamerules.DO_BABY_ZOMBIE_SPAWNING.getRule())) {
            ((ZombieEntity) entity).setChild(false);
        } else if (entity instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) entity;
            player.getAttribute(Attributes.MAX_HEALTH).setBaseValue(entity.getEntityWorld().getGameRules().getInt(MoreGamerules.MAX_HEALTH.getRule()));
            if (player.getHealth() > player.getMaxHealth())
                player.setHealth(player.getMaxHealth());
        }
    }

    @SubscribeEvent
    public void onPlayerTick(TickEvent.PlayerTickEvent event) {
        if (event.side.isServer() && event.phase == TickEvent.Phase.START) {
            PlayerEntity player = event.player;
            if (player.getEntityWorld().getGameRules().getBoolean(MoreGamerules.DISABLE_HUNGER.getRule())) {
                FoodStats stats = player.getFoodStats();

                // Regen food
                stats.setFoodLevel((int) Math.min(stats.getFoodLevel() + 1.0F, 20.0F));

                // Give 1 saturation to stop hunger bar jitter
                if (!stats.needFood() && !player.shouldHeal() && stats.getSaturationLevel() == 0) stats.setFoodSaturationLevel(1.0F);
            }
        }
    }

    @SubscribeEvent
    public void onCommand(CommandEvent event) {
        String[] cmd = event.getParseResults().getReader().getRead().split(" ");
        if (cmd[0].equalsIgnoreCase("/gamerule") && cmd[1].equalsIgnoreCase(MoreGamerules.MAX_HEALTH.getName())) {
            ServerWorld world = event.getParseResults().getContext().getSource().getWorld();
            if (cmd.length > 2) {
                int val = Integer.parseInt(cmd[2]);
                if (val >= 1 && val <= 1024) {
                    for (ServerPlayerEntity player : world.getPlayers()) {
                        player.getAttribute(Attributes.MAX_HEALTH).setBaseValue(val);
                        if (player.getHealth() > player.getMaxHealth())
                            player.setHealth(player.getMaxHealth());
                    }
                }
            }
        }
    }

}
